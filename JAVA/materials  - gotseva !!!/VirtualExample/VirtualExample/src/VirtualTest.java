
public class VirtualTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		A objA = new A();
		B objB = new B();
		A objAB = new B();
		
		objA.X();
		objB.X();
		objAB.X();
	}

}

class A {
	public void X() {
		System.out.println("In A.X");
		inc();
	}
	public void inc() {
		System.out.println("In A.inc");
		Y();
	}
	public void Y() {
		System.out.println("In A.Y");
	}
}

class B extends A {
	public void X() {
		System.out.println("In B.X");
		inc();
	}
	public void Y() {
		System.out.println("In B.Y");
	}
	
}