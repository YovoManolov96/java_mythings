import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Random;

public class Test {

	private int correctAnswers;
	ArrayList<Question> questions;

	public Test(ArrayList<Question> questions) {
		this.questions = questions;
		this.correctAnswers = 0;
	}

	public void performTest() {
		for (int i = 0; i < this.questions.size(); i++) {
			questions.get(i).askQuestion();
			if (questions.get(i).checkQuestion()) {
				correctAnswers++;
			}
		}
	}

	public int numCorrectAnswers() {
		return this.correctAnswers;
	}

	public int[] generateRandomNumbers() {
		Random randomGenerator = new Random();
		int randomNumber;
		int[] numbers = new int[questions.size()];

		for (int i = 0; i < numbers.length; i++) {
			numbers[i] = randomGenerator.nextInt(numbers.length); // generates
																	// random
																	// number in
																	// bounds 0
																	// -
																	// numbers.length
		}

		return numbers;

	}

	public void shuffle() {
		int[] randomNumbers = generateRandomNumbers();

		for (int i = 0; i < questions.size(); i++) {
			Collections.swap(this.questions, i, randomNumbers[i]);
		}

	}

}
