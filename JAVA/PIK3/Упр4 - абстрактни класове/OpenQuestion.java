public class OpenQuestion extends Question {

	private String correctAnswer;
	private String currectAnswer;

	public OpenQuestion(String question, String correctAnswer) {
		super(question);
		this.correctAnswer = correctAnswer;
		this.currectAnswer = new String();
	}

	public String getCorrectAnswer() {
		return correctAnswer;
	}

	public void setCorrectAnswer(String correctAnswer) {
		this.correctAnswer = correctAnswer;
	}

	public String getCurrectAnswer() {
		return currectAnswer;
	}

	public void setCurrectAnswer(String currectAnswer) {
		if (currectAnswer.length() <= 255)
			this.currectAnswer = currectAnswer;
	}

	@Override
	public void saveAnswer(String answer) {
		this.currectAnswer = answer;
	}

	@Override
	public boolean checkQuestion() {
		return correctAnswer.equalsIgnoreCase(currectAnswer);
	}

}
