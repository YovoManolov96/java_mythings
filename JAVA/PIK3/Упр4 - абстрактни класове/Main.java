import java.util.ArrayList;
import java.util.Scanner;


public class Main {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String question, answer;
		int answerInt;
		
		ArrayList<Question> questions = new ArrayList();
		
		for (int i = 0; i < 3; i++) {
			System.out.println("Question:");
			question = sc.nextLine();
			if(i%2 == 0){
				System.out.println("The correct answer is:");
				answer = sc.nextLine();
				questions.add(new OpenQuestion(question, answer));
			}else{
				System.out.println("The correct answer is(int):");
				answerInt = Integer.parseInt(sc.nextLine());
				questions.add(new IntegerQuestion(question, answerInt));
			}
		}
		
		Test testPIK3 = new Test(questions);
		
		testPIK3.shuffle();
		testPIK3.performTest();
		System.out.println("You have " + testPIK3.numCorrectAnswers() + " correct answers." );
		
		
		
	}
	
}
