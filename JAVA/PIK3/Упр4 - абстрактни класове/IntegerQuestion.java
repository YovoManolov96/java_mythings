
public class IntegerQuestion extends Question {
	private int correctAnswer;
	private int currentAnswer;
	
	public IntegerQuestion(String question, int correctAnswer) {
		super(question);
		this.correctAnswer = correctAnswer;
		currentAnswer = 0;
	}
	
	
	
	public int getCorrectAnswer() {
		return correctAnswer;
	}

	public void setCorrectAnswer(int correctAnswer) {
		this.correctAnswer = correctAnswer;
	}

	public int getCurrentAnswer() {
		return currentAnswer;
	}

	public void setCurrentAnswer(int currentAnswer) {
		this.currentAnswer = currentAnswer;
	}

	@Override
	public void saveAnswer(String answer) {
		this.correctAnswer = Integer.parseInt(answer);		
	}

	@Override
	public boolean checkQuestion() {
		return correctAnswer == currentAnswer;
	}
	
	
}
