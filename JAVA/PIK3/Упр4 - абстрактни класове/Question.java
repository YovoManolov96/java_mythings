import java.util.Scanner;


public abstract class Question {
	
	private String question;
		
	public Question(String question) {
		setQuestion(question);
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		if(question.length() <= 100){
			this.question = question;
		}
	}
	
	public void askQuestion(){
		Scanner sc = new Scanner(System.in);
		String answer;
		
		System.out.println(this.question);
		System.out.println("Your answer:");
		answer = sc.nextLine();
		
		saveAnswer(answer);
	}
	
	public abstract void saveAnswer(String answer);
	public abstract boolean checkQuestion();
	
	
}
