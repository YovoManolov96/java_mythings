
/*
 * 
 * ��������� ������ � ��������� ��������� � ������� ��������� �� �������� ������:
 * ������ 1.
 * �� ������� String, �� �� ����� ���� �� ������� � ����.
 
 * ������ 2.
 * ����� � String �������� �����, ���� ���� ������������� �� ��������� ����� ����� ��:
 * ������: �-55+35-41+21-8+3�
 * �� �� �������� � ������� �������.
 
 * ������ 3.
 * �� �������� 2 String-� �� �� ������ � �� �� ����� ���� �������� ����� ���� ������ �� ������� ��� ������.
 
 * ������ 4.
 * �� �� �������� � ������ ���� �������� �������� �� ����������� ��������� �� � ������� ���� ������ ��������� String.

 */

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
	public static final String task1 = "Your grade is between 2 and 6. The date is 12.12.12";
	public static final String task2 = "-3+55-55+3-6";
	public static final String task3 = "How many times there is the word many?";
	public static final String task4 = "Use short sentences to create punch and make a point."
			+ " Use phrases and even words as sentences. Really.";

	public static void main(String[] args) {

		System.out.println("Result from countDigits():" + countDigits(task1));
		System.out.println("Result from sumString():" + sumString(task2));
		System.out.println("Result from countWords():" + countWords(task3, "many"));
		System.out.println("Result from countSentances()" + countSentances(task4));
	}

	// task 1
	public static int countDigits(String str) {
		int total = 0;

		String[] numbers = str.split("\\D+");
		for (int i = 0; i < numbers.length; i++) {
			if (!numbers[i].isEmpty())
				total++;
		}

		return total;

	}

	// task 2
	public static int sumString(String obj) {
		int result = 0;

		Matcher m = Pattern.compile("[\\+-][0-9]+").matcher(obj);
		while (m.find()) {
			result += Integer.parseInt(m.group());
		}

		return result;
	}
	
	//task 3
	public static int countWords(String sentances, String word) {
		int total = 0;

		Matcher m = Pattern.compile(word).matcher(sentances);
		while (m.find()) {
			total++;
		}

		return total;
	}

	//task4
	public static int countSentances(String sentances) {
		int total = 0;

		Matcher m = Pattern.compile("[\\.?!]").matcher(sentances);

		while (m.find()) {
			total++;
		}

		return total;

	}

}
