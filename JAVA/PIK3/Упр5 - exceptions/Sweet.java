package sweetfactory;


public class Sweet {
    
    private String name;
    private int neededSugar;
    private int neededFlour;
    private int neededColor;
    private int neededFilling;

    public Sweet(String name, int neededSugar, int neededFlour, int neededColor, int neededFilling) {
        setName(name);
        setNeededSugar(neededSugar);
        setNeededColor(neededColor);
        setNeededFilling(neededFilling);
        setNeededFlour(neededFlour);
    }

    public Sweet() {
        this.name = "Apple pie";
        this.neededColor = 10;
        this.neededFilling = 30;
        this.neededFlour = 30;
        this.neededSugar = 100;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNeededSugar() {
        return neededSugar;
    }

    public void setNeededSugar(int neededSugar) {
        if(neededSugar > 0)
        this.neededSugar = neededSugar;
    }

    public int getNeededFlour() {
        return neededFlour;
    }

    public void setNeededFlour(int neededFlour) {
        if(neededFlour > 0)
        this.neededFlour = neededFlour;
    }

    public int getNeededColor() {
        return neededColor;
    }

    public void setNeededColor(int neededColor) {
        if(neededColor > 0)
        this.neededColor = neededColor;
    }

    public int getNeededFilling() {
        return neededFilling;
    }

    public void setNeededFilling(int neededFilling) {
        if(neededFilling > 0)
        this.neededFilling = neededFilling;
    }

    @Override
    public String toString() {
        return "Name: " + this.name +
                "\nNeeded sugar: " + this.neededColor +
                "\nNeeded filling: " + this.neededFilling +
                "\nNeeded flour: " + this.neededFlour +
                "\nNeeded color: " + this.neededColor;
    }
    
    
    
}
