
package sweetfactory;

public class FactoryException extends Exception {
    
    public FactoryException(String err){
        super(err);
    }
    
}
