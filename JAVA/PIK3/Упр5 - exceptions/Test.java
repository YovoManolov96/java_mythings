package sweetfactory;

import java.util.ArrayList;

public class Test {
  
    public static void main(String[] args) {
        ArrayList<Sweet> listToDo = new ArrayList();
        
        for (int i = 0; i < 1000; i++) {
            listToDo.add(new Sweet());
        }
        
        
        SweetFactory factory = new SweetFactory(listToDo, 2.0, 2.0, 2.0, 2.0);
        
        try{
            factory.startProductionLine();
             factory.startProductionLine();
        }catch(FactoryException e){
            System.err.println(e.getMessage());
        }
        
    }
    
}
