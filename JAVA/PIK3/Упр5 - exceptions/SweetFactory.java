package sweetfactory;

import java.util.ArrayList;

public class SweetFactory implements FactoryInterface {

    private ArrayList<Sweet> listToMake;
    private boolean isWorking;
    private double sugar;
    private double flour;
    private double color;
    private double filling;

    public SweetFactory(ArrayList<Sweet> listToMake, double sugar, double flour,
            double color, double filling) {
        setSugar(sugar);
        setColor(color);
        setFilling(filling);
        setFlour(flour);
        setListToMake(listToMake);
        this.isWorking = false;
    }

    public ArrayList<Sweet> getListToMake() {
        return listToMake;
    }

    public void setListToMake(ArrayList<Sweet> listToMake) {
        this.listToMake = listToMake;
    }

    public boolean isIsWorking() {
        return isWorking;
    }

    public void setIsWorking(boolean isWorking) {
        this.isWorking = isWorking;
    }

    public double getSugar() {
        return sugar;
    }

    public void setSugar(double sugar) {
        if (sugar > 0) {
            this.sugar = sugar;
        }
    }

    public double getFlour() {
        return flour;
    }

    public void setFlour(double flour) {
        if (flour > 0) {
            this.flour = flour;
        }
    }

    public double getColor() {
        return color;
    }

    public void setColor(double color) {
        if (color > 0) {
            this.color = color;
        }
    }

    public double getFilling() {
        return filling;
    }

    public void setFilling(double filling) {
        if (filling > 0) {
            this.filling = filling;
        }
    }

    @Override
    public void startProductionLine() throws FactoryException {
        if (isProductionLineWorking()) {
            throw new FactoryException("Production line already working!");
        }
        
        this.isWorking = true;
        
        while (!this.listToMake.isEmpty()) {
            if (this.color < listToMake.get(0).getNeededColor() / 1000d) {
                throw new FactoryException("Not enough color!");
            } else if (this.filling < listToMake.get(0).getNeededFilling() / 1000d) {
                throw new FactoryException("Not enough filling!");
            } else if (this.flour < listToMake.get(0).getNeededFlour() / 1000d) {
                throw new FactoryException("Not enough flour!");
            } else if (this.sugar < listToMake.get(0).getNeededSugar() / 1000d) {
                throw new FactoryException("Not enough sugar!");
            }
            
            this.color -= listToMake.get(0).getNeededColor() / 1000d;
            this.filling -= listToMake.get(0).getNeededFilling() / 1000d;
            this.flour -= listToMake.get(0).getNeededFlour() / 1000d;
            this.sugar -= listToMake.get(0).getNeededSugar() / 1000d;
            
            this.listToMake.remove(0);
            
        }
    }
    
    
    
    @Override
    public void stopProductionLine() {
        this.isWorking = false;
    }

    @Override
    public boolean isProductionLineWorking() {
        return isWorking;
    }

}
