package sweetfactory;

public interface FactoryInterface {
    
    public void startProductionLine() throws FactoryException ;
    public void stopProductionLine();
    public boolean isProductionLineWorking();
    
    
}
