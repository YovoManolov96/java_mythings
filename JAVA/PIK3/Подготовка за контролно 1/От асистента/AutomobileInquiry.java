
package automobileinquiry;

public class AutomobileInquiry {

    private final String owner;
    private final String newOwner;
    private final String regNumber;
    private long inNumber;

    public AutomobileInquiry(String owner, String newOwner, String regNumber) {
        this.owner = owner;
        this.newOwner = newOwner;
        this.regNumber = regNumber;
        this.inNumber = 0;
    }
    
    public long getInNumber() {
        return inNumber;
    }

    public void setInNumber(long inNumber) {
        if(inNumber >= 0)
        this.inNumber = inNumber;
    }

    public String getOwner() {
        return owner;
    }

    public String getNewOwner() {
        return newOwner;
    }

    public String getRegNumber() {
        return regNumber;
    }
    
    public String getRegCity(){
        
        if(regNumber.length() == 7){
            return regNumber.substring(0,1);
        }else if(regNumber.length() == 8){
            return regNumber.substring(0,2);
        }
        return "INVALID_DATA";
    }
    
}
