

package automobileinquiry;

import java.util.ArrayList;

public class Worker {
    private MVRCSSofia payDesk;
    private String name;
    private static long totalChanged = 0L;
    private long changedRegistrationPlate;

    public Worker(MVRCSSofia payDesk, String name, long changedRegistrationPlate) {
        this.payDesk = payDesk;
        this.name = name;
        this.changedRegistrationPlate = changedRegistrationPlate;
    }
    
    public void changeRegistrationPlate(){
        int index = findLowestNumber(MVRCSSofia.automobileInquiry);
        
        try{
            MVRCSSofia.automobileInquiry.remove(index);
        }catch (java.lang.IndexOutOfBoundsException e){
            System.out.println("There is no more automobile inquiry.");
        }
        
        ++changedRegistrationPlate;
        ++totalChanged;
        System.out.println("Request is done.");
    }
    
    public int findLowestNumber(ArrayList<AutomobileInquiry> automobileInquiry){
        long min = Long.MAX_VALUE;
        int index = 0;
        int counter = 0;
        for (AutomobileInquiry inquiry : automobileInquiry) {
            counter++;
            if(min > inquiry.getInNumber()){
                min = inquiry.getInNumber();
                index =counter;
            }
        }
        return index;
    }
    
}
