package propertyinquiry;

public class CustomerServiceSofia extends CustomerService {
    
    protected String city = "Sofia";
    protected String obshtina ;
    
    public CustomerServiceSofia(int numGishe, String obshtina) {
        super(numGishe);
        this.obshtina = obshtina;
    }
    

    public static int addInquiry(PropertyInquiry newProperty){
        
        if(newProperty.city.equalsIgnoreCase("Sofia")){
            newProperty.setInNumber(++currentNum);
            requests.add(newProperty);
        }
        
        
        return currentNum;
    }
    
}
