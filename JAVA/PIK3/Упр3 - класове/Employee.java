package propertyinquiry;

public class Employee {

    protected String name;
    protected String egn;
    protected int otdNum;

    public Employee(String name, String egn, int otdNum) {
        try {
            setEgn(egn);
        } catch (Exception ex) {
            System.err.println(ex.getMessage());
            this.egn = "0000000000";
        }
        setName(name);
        setOtdNum(otdNum);
    }

    public Employee() {
        this.egn = "1234567890";
        this.name = "John";
        this.otdNum = 3;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEgn(String egn) throws Exception {
        if (egn.length() == 10) {
            this.egn = egn;
        } else {
            throw new Exception("EGN not 10 symbols");
        }
    }

    public void setOtdNum(int otdNum) {
        this.otdNum = otdNum;
    }

    public String getName() {
        return name;
    }

    public String getEgn() {
        return egn;
    }

    public int getOtdNum() {
        return otdNum;
    }

}
