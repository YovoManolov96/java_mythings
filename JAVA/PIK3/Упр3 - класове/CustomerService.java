
package propertyinquiry;


import java.util.*;


public class CustomerService {
   public static ArrayList<PropertyInquiry> requests = new ArrayList();
    protected int numGishe;
    protected Employee employeeOnDuty;
    protected static int currentNum = 0;

    public CustomerService(int numGishe) {
        this.numGishe = numGishe;
        employeeOnDuty = new Employee();
        currentNum = 0;
    }
    
    public static int addInquiry(PropertyInquiry newProperty){
        newProperty.setInNumber(++currentNum);
        requests.add(newProperty);
        
        return currentNum;
    }
    
    public static void deleteInquiry(int inNum){
        for(int counter = 0 ; counter < requests.size() ; counter++){
            if(requests.get(counter).getInNumber() == inNum){
                requests.remove(counter);
            }
        }
    }

    public void setEmployeeOnDuty(Employee employeeOnDuty) {
        this.employeeOnDuty = employeeOnDuty;
    }
    
    public void print(){
        for(int counter = 0 ; counter < requests.size() ; counter++){
            System.out.println(requests.get(counter).uniqueNum );
            
        }
    }
    
}
