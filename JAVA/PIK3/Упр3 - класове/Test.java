package propertyinquiry;

public class Test {

    public static void main(String[] args) {
        Employee firstShift = null;
        firstShift = new Employee("John", "1234567890", 3);

        CustomerServiceSofia abv = new CustomerServiceSofia(1, "Sofia");
        abv.setEmployeeOnDuty(firstShift);
        PropertyInquiry building = new PropertyInquiry("Petko", "Petko", 1, "Sofia");
        PropertyInquiry building2 = new PropertyInquiry("Stefan", "Yanko", 2, "Sofia");

        CustomerServiceSofia.addInquiry(building);
        CustomerServiceSofia.addInquiry(building2);

        System.out.println("Списъка след добавянето на 2 заявки:");

        for (int i = 0; i < CustomerServiceSofia.requests.size(); i++) {
            System.out.println(CustomerServiceSofia.requests.get(i).toString() + "\n");
            
        }

        CustomerServiceSofia.deleteInquiry(1);

        System.out.println("Списъка след изтриването на заявка 1:");

        for (int i = 0; i < CustomerServiceSofia.requests.size(); i++) {
            System.out.println(CustomerServiceSofia.requests.get(i).toString() + "\n");
        }
    }
}
