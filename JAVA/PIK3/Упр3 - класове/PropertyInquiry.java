
package propertyinquiry;

public class PropertyInquiry {
    
    protected final String owner;
    protected final String currentOwner;
    protected final int uniqueNum;
    protected final String city;
    protected int inNumber = 0;

    public PropertyInquiry(String owner, String currentOwner, int uniqueNum, String city) {
        this.owner = owner;
        this.currentOwner = currentOwner;
        this.uniqueNum = uniqueNum;
        this.city = city;
        this.inNumber = 0;
    }
    
    
    public String getOwner() {
        return owner;
    }

    public String getCurrentOwner() {
        return currentOwner;
    }

    public int getUniqueNum() {
        return uniqueNum;
    }

    public String getCity() {
        return city;
    }

    public int getInNumber() {
        return inNumber;
    }   

    public void setInNumber(int inNumber) {
        this.inNumber = inNumber;
    }

    @Override
    public String toString() {
        return "Current owner: " + this.currentOwner +
                "\nNew Owner:" + this.owner +
                "\nCity:" + this.city +
                "\nInput number:" + inNumber + 
                "\nUnique number:" + this.uniqueNum;
    }
   
    
    
}
