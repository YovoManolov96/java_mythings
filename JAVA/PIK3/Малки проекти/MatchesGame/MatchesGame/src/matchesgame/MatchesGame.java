/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 /*Иван и Петър играят на комбинаторна игра. Пред тях има една купчина с кибритени клечки. Първо Иван си избира произволно цяло
  число N между 9 и 12, след което подрежда N клечки една до друга на масата. След това Петър си намисля цяло число K от 3 до N/3 + 1.
Играта започва първи Иван. Той има право да вземе от 1 до K на брой клечки. След като ги вземе, на ход е Петър – той също има
право да вземе от 1 до K на брой клечки. После пак е Иван и т.н. Играта продължава докато има клечки на масата. Печели играчът,
 който е взел последната клечка.*/

package matchesgame;

import java.util.Iterator;
import java.util.LinkedList;
import javax.swing.JLabel;

/**
 *
 * @author Philip
 */
public class MatchesGame extends javax.swing.JFrame {

    int N; ///произволно цяло число N между 9 и 12
    int K; ///цяло число K от 3 до N/3 + 1.

    LinkedList<JLabel> sticks;
    boolean player1OnMove = true;

    /**
     * Creates new form MatchesGame
     */
    public MatchesGame() {
        initComponents();
        clearSticks(12);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        startPanel = new javax.swing.JPanel(); ///JPanel is a generic lightweight container.
        NPanel = new javax.swing.JPanel();
        NLabel = new javax.swing.JLabel();     ///A display area for a short text string or an image, or both.
        NValue = new javax.swing.JTextField(); ///JTextField is a lightweight component that allows the editing of a SINGLE line of text.
        KPanel = new javax.swing.JPanel();
        KLabel = new javax.swing.JLabel();
        KValue = new javax.swing.JTextField();
        startButtonPanel = new javax.swing.JPanel();
        startButton = new javax.swing.JButton(); /// An implementation of a "push" button.
        footerPanel = new javax.swing.JPanel();
        message = new javax.swing.JLabel();
        sticksPanel = new javax.swing.JPanel();
        stick1 = new javax.swing.JLabel();
        stick2 = new javax.swing.JLabel();
        stick3 = new javax.swing.JLabel();
        stick4 = new javax.swing.JLabel();
        stick5 = new javax.swing.JLabel();
        stick6 = new javax.swing.JLabel();
        stick7 = new javax.swing.JLabel();
        stick8 = new javax.swing.JLabel();
        stick9 = new javax.swing.JLabel();
        stick10 = new javax.swing.JLabel();
        stick11 = new javax.swing.JLabel();
        stick12 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(460, 300));

        startPanel.setLayout(new java.awt.GridLayout(1, 3));

        NLabel.setText("N");
        NPanel.add(NLabel);

        NValue.setText("11");
        NPanel.add(NValue);

        startPanel.add(NPanel);

        KLabel.setText("K");
        KPanel.add(KLabel);

        KValue.setText("3");
        KPanel.add(KValue);

        startPanel.add(KPanel);

        startButton.setText("Старт");
        startButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startButtonActionPerformed(evt);
            }
        });
        startButtonPanel.add(startButton);

        startPanel.add(startButtonPanel);

        getContentPane().add(startPanel, java.awt.BorderLayout.PAGE_START);

        message.setText("Въведете N и K и натиснете Старт");
        footerPanel.add(message);

        getContentPane().add(footerPanel, java.awt.BorderLayout.PAGE_END);

        stick1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/matchesgame/matchstick.jpg"))); // NOI18N
        stick1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                stick1MouseClicked(evt);
            }
        });
        sticksPanel.add(stick1);

        stick2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/matchesgame/matchstick.jpg"))); // NOI18N
        stick2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                stick2MouseClicked(evt);
            }
        });
        sticksPanel.add(stick2);

        stick3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/matchesgame/matchstick.jpg"))); // NOI18N
        stick3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                stick3MouseClicked(evt);
            }
        });
        sticksPanel.add(stick3);

        stick4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/matchesgame/matchstick.jpg"))); // NOI18N
        stick4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                stick4MouseClicked(evt);
            }
        });
        sticksPanel.add(stick4);

        stick5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/matchesgame/matchstick.jpg"))); // NOI18N
        stick5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                stick5MouseClicked(evt);
            }
        });
        sticksPanel.add(stick5);

        stick6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/matchesgame/matchstick.jpg"))); // NOI18N
        stick6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                stick6MouseClicked(evt);
            }
        });
        sticksPanel.add(stick6);

        stick7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/matchesgame/matchstick.jpg"))); // NOI18N
        stick7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                stick7MouseClicked(evt);
            }
        });
        sticksPanel.add(stick7);

        stick8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/matchesgame/matchstick.jpg"))); // NOI18N
        stick8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                stick8MouseClicked(evt);
            }
        });
        sticksPanel.add(stick8);

        stick9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/matchesgame/matchstick.jpg"))); // NOI18N
        stick9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                stick9MouseClicked(evt);
            }
        });
        sticksPanel.add(stick9);

        stick10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/matchesgame/matchstick.jpg"))); // NOI18N
        stick10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                stick10MouseClicked(evt);
            }
        });
        sticksPanel.add(stick10);

        stick11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/matchesgame/matchstick.jpg"))); // NOI18N
        stick11.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                stick11MouseClicked(evt);
            }
        });
        sticksPanel.add(stick11);

        stick12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/matchesgame/matchstick.jpg"))); // NOI18N
        stick12.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                stick12MouseClicked(evt);
            }
        });
        sticksPanel.add(stick12);

        getContentPane().add(sticksPanel, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void clearSticks(int M) {
        for (int i = 1; i <= M; i++) {
            switch (i) {
                case 1:
                    stick1.setVisible(false);
                    break;
                case 2:
                    stick2.setVisible(false);
                    break;
                case 3:
                    stick3.setVisible(false);
                    break;
                case 4:
                    stick4.setVisible(false);
                    break;
                case 5:
                    stick5.setVisible(false);
                    break;
                case 6:
                    stick6.setVisible(false);
                    break;
                case 7:
                    stick7.setVisible(false);
                    break;
                case 8:
                    stick8.setVisible(false);
                    break;
                case 9:
                    stick9.setVisible(false);
                    break;
                case 10:
                    stick10.setVisible(false);
                    break;
                case 11:
                    stick11.setVisible(false);
                    break;
                case 12:
                    stick12.setVisible(false);
                    break;
            }
        }
    }

    private void getSticks(JLabel stick) {
        if (sticks.indexOf(stick) < this.K) {
            Iterator it = this.sticks.iterator();
            while (it.hasNext()) {
                JLabel current = (JLabel) (it.next());
                current.setVisible(false);
                it.remove();
                if (current.equals(stick)) {
                    break;
                }
            }
        } else {
            return;
        }
        if(this.sticks.isEmpty()){
            this.message.setText(this.message.getText().substring(0,7)+" спечели!");
        }
        else if (this.player1OnMove == true) {
            this.player1OnMove = false;
            this.message.setText("Играч 2 е на ход");
        } else {
            this.player1OnMove = true;
            this.message.setText("Играч 1 е на ход");
        }
    }

    private void startButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startButtonActionPerformed
        clearSticks(12);
        try {
            this.N = Integer.parseInt(NValue.getText());
            this.K = Integer.parseInt(KValue.getText());

            if (this.N < 9 || this.N > 12 || this.K < 3 || this.K > (this.N / 3 + 1)) {
                message.setText("N трябва да е число от 9 до 12, а K число от 3 до N/3 + 1");
                return;
            }
        } catch (Exception e) {
            message.setText("N и K трябва да са цели числа!");
            return;
        }

        this.sticks = new LinkedList<>();

        for (int i = 1; i <= this.N; i++) {
            switch (i) {
                case 1:
                    this.sticks.add(stick1);
                    stick1.setVisible(true);
                    break;
                case 2:
                    this.sticks.add(stick2);
                    stick2.setVisible(true);
                    break;
                case 3:
                    this.sticks.add(stick3);
                    stick3.setVisible(true);
                    break;
                case 4:
                    this.sticks.add(stick4);
                    stick4.setVisible(true);
                    break;
                case 5:
                    this.sticks.add(stick5);
                    stick5.setVisible(true);
                    break;
                case 6:
                    this.sticks.add(stick6);
                    stick6.setVisible(true);
                    break;
                case 7:
                    this.sticks.add(stick7);
                    stick7.setVisible(true);
                    break;
                case 8:
                    this.sticks.add(stick8);
                    stick8.setVisible(true);
                    break;
                case 9:
                    this.sticks.add(stick9);
                    stick9.setVisible(true);
                    break;
                case 10:
                    this.sticks.add(stick10);
                    stick10.setVisible(true);
                    break;
                case 11:
                    this.sticks.add(stick11);
                    stick11.setVisible(true);
                    break;
                case 12:
                    this.sticks.add(stick12);
                    stick12.setVisible(true);
                    break;
            }
        }

        this.message.setText("Играч 1 е на ход");
    }//GEN-LAST:event_startButtonActionPerformed

    private void stick1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stick1MouseClicked
        this.getSticks(stick1);
    }//GEN-LAST:event_stick1MouseClicked

    private void stick2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stick2MouseClicked
        this.getSticks(stick2);
    }//GEN-LAST:event_stick2MouseClicked

    private void stick3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stick3MouseClicked
        this.getSticks(stick3);
    }//GEN-LAST:event_stick3MouseClicked

    private void stick4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stick4MouseClicked
        this.getSticks(stick4);
    }//GEN-LAST:event_stick4MouseClicked

    private void stick5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stick5MouseClicked
        this.getSticks(stick5);
    }//GEN-LAST:event_stick5MouseClicked

    private void stick6MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stick6MouseClicked
        this.getSticks(stick6);
    }//GEN-LAST:event_stick6MouseClicked

    private void stick7MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stick7MouseClicked
        this.getSticks(stick7);
    }//GEN-LAST:event_stick7MouseClicked

    private void stick8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stick8MouseClicked
        this.getSticks(stick8);
    }//GEN-LAST:event_stick8MouseClicked

    private void stick9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stick9MouseClicked
        this.getSticks(stick9);
    }//GEN-LAST:event_stick9MouseClicked

    private void stick10MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stick10MouseClicked
        this.getSticks(stick10);
    }//GEN-LAST:event_stick10MouseClicked

    private void stick11MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stick11MouseClicked
        this.getSticks(stick11);
    }//GEN-LAST:event_stick11MouseClicked

    private void stick12MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_stick12MouseClicked
        this.getSticks(stick12);
    }//GEN-LAST:event_stick12MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MatchesGame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MatchesGame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MatchesGame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MatchesGame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MatchesGame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel KLabel;
    private javax.swing.JPanel KPanel;
    private javax.swing.JTextField KValue;
    private javax.swing.JLabel NLabel;
    private javax.swing.JPanel NPanel;
    private javax.swing.JTextField NValue;
    private javax.swing.JPanel footerPanel;
    private javax.swing.JLabel message;
    private javax.swing.JButton startButton;
    private javax.swing.JPanel startButtonPanel;
    private javax.swing.JPanel startPanel;
    private javax.swing.JLabel stick1;
    private javax.swing.JLabel stick10;
    private javax.swing.JLabel stick11;
    private javax.swing.JLabel stick12;
    private javax.swing.JLabel stick2;
    private javax.swing.JLabel stick3;
    private javax.swing.JLabel stick4;
    private javax.swing.JLabel stick5;
    private javax.swing.JLabel stick6;
    private javax.swing.JLabel stick7;
    private javax.swing.JLabel stick8;
    private javax.swing.JLabel stick9;
    private javax.swing.JPanel sticksPanel;
    // End of variables declaration//GEN-END:variables
}
