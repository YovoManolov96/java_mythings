/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import java.io.FileOutputStream;

/**
 *
 * @author yovo
 */
public class JavaApplication1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try(FileOutputStream fout = new FileOutputStream("yovoTest.txt")) { 
        String str = "yovo";
        byte barray [] = str.getBytes();
        fout.write(barray);
        fout.close();
        }catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
    }
    
}
