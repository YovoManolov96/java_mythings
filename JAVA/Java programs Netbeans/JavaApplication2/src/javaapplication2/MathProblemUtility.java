/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author yovo
 */
/*Няма конструктор и член променливи,
 а съдържа един единствен не-private статичен метод parseFile с входен параметър 
 String – име на файл – и изходен параметър масив от задачи.*/

/*Отвраря файла и преброява колко задачи има в него. Записва това в променлива int problemsCount;
 Създава масив MathProblem[] problems с брой елементи problemsCount;
 Обхожда файла и записва всяка задача в масива. Ако за дадена задача се получи 
 проблем с информацията (нещо във формата на файла не отговаря), задачата да бъде пропусната;
 Ако не е запълнен целия масив, да се премахнат празните клетки;
 Връща масива.*/
class MathProblemUtility {

    public MathProblem[] parseFile(String FileName) {
        int problemsCountVar = 0;
        try {
            //Отвраря файла и преброява колко задачи има в него. Записва това в променлива int problemsCount;
            problemsCountVar = problemsCount("E:\\JAVA\\Java programs Netbeans\\JavaApplication2\\file.txt");
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        
        MathProblem [] mp = new MathProblem [problemsCountVar];
         for (int i = 0; i < problemsCountVar; i++) {
                       
       File file = new File("E:\\JAVA\\Java programs Netbeans\\JavaApplication2\\file.txt");
 
       for (int i = 0; i < problemsCountVar; i++) {
           
          String name = " ";
            String family_name = " ";
            String Subject = " ";
            int Class = 0;
            String distractors = " ";
            String topic = " ";
            String description = " ";
            String trueAnswer = " ";
            
           Scanner fsc = new Scanner(file);
       
            {
                //Author threatment
                 String author = fsc.nextLine();
                 String [] authorStr =  author.split(" ");
                 name = authorStr[1];
                 family_name = authorStr[2];

            }
            
            {
                //Subject threatment
                 String subject = fsc.nextLine();
                 String [] subjectStr =  subject.split(" ");
                 Subject = subjectStr[2];
            }
            
            {
                //Class threatment
                 String Class_str = fsc.nextLine();
                 Scanner scStr = new Scanner(Class_str);
                 int a = scStr.nextInt();
                 Class = a;
                 
            }
            
            {
                //Topic threatment
                 String Topic_str = fsc.nextLine();
                 String [] topicStr = Topic_str.split(" ");
                 for( int a = 1 ; a <= topicStr.length ;a++){
                     topic +=  ( " " + topicStr[a]);
                 }
                 
            }
            
            {
                //Description threatment
                 String Descrition_str = fsc.nextLine();
                 String [] descriptionStr = Descrition_str.split(" ");
                 for( int a = 1 ; a <= descriptionStr.length ;a++){
                     description +=  ( " " + topicStr[a]);
                 }
                 
            }
            {
                //thrueAnswer threatment
                 String TrueAnswer_str = fsc.nextLine();
                 String [] trueAnswerStr = TrueAnswer_str.split("=");
                 for( int a = 1 ; a <= trueAnswerStr.length ;a++){
                     trueAnswer +=  ( " " + trueAnswerStr[a]);
                 }
                 
            }
       
                /*Автор: Иван Иванов
                Предмет: Математика
                Клас: 8
                Тема: Квадратни уравнения
                Условие: Решете уравнението x^2+2x-3=0
                Верен отговор: x1 = -3, x2 = 1
                Дистрактор 1: x1=3, x2=1
                Дистрактор 2: x1=3, x2=-1
                Дистрактор 3: x1=-3, x2=-1
                Дистрактор 4: x1=1, x2=1

               .
       
               */
       
       
     return mp;
    }

    private static int problemsCount (String FileName) throws IOException {
        Scanner sc1 = new Scanner(new FileReader(FileName));
        int count;
        count = 0;
        while (sc1.hasNext()) {
            if (sc1.nextLine().equals(".")) {
                count++;
            }
        }
        sc1.close();
        return count;
    }
}
